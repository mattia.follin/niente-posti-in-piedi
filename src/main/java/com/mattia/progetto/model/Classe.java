package com.mattia.progetto.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Classe {

    @Id
    @GeneratedValue
    private Long idC;
    private int numeroStanza;
    private int pianoStanza;
    private int numeroPersone;


    public Long getIdC() {
        return idC;
    }

    public void setIdC(Long idC) {
        this.idC = idC;
    }

    public int getNumeroStanza() {
        return numeroStanza;
    }

    public void setNumeroStanza(int numeroStanza) {
        this.numeroStanza = numeroStanza;
    }

    public int getPianoStanza() {
        return pianoStanza;
    }

    public void setPianoStanza(int pianoStanza) {
        this.pianoStanza = pianoStanza;
    }

    public int getNumeroPersone() {
        return numeroPersone;
    }

    public void setNumeroPersone(int numeroPersone) {
        this.numeroPersone = numeroPersone;
    }
}

