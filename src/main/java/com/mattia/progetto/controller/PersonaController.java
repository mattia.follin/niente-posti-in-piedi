package com.mattia.progetto.controller;

import com.mattia.progetto.model.RequestLogin;
import com.mattia.progetto.repository.PersonaRepository;
import com.mattia.progetto.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class PersonaController {

    private final PersonaRepository personaRepository;

    @Autowired
    PersonaController(PersonaRepository repository){
        personaRepository = repository;
    }

    @GetMapping("/persone")
     Iterable<Persona> getPersone() {
        return personaRepository.findAll();
    }

    @GetMapping("/persone/{id}")
    Persona getPersona(@PathVariable Long id) {
        return personaRepository.findById(id).orElseThrow();
    }

    @PostMapping("/persone")
    Persona createPersona(@RequestBody Persona newPersona) {
        return personaRepository.save(newPersona);
    }

    @PutMapping("/persone/{id}")
    Persona updatePersona(@PathVariable Long id, @RequestBody Persona personaDto){
        Persona personaToUpdate = personaRepository.findById(id).orElseThrow();
        personaToUpdate.setNome(personaDto.getNome());
        personaToUpdate.setCognome(personaDto.getCognome());
        personaToUpdate.setRuolo(personaDto.getRuolo());
        personaToUpdate.setEmail(personaDto.getEmail());
        personaToUpdate.setPassword(personaDto.getPassword());
        return personaRepository.save(personaToUpdate);
    }

    @DeleteMapping("/persone/{id}")
    void deletePersona(@PathVariable Long id) {
        Persona persona = personaRepository.findById(id).orElseThrow();
        personaRepository.delete(persona);
    }

    @PostMapping("/login")
    boolean loginUtente(@RequestBody RequestLogin requestLogin){
        Optional<Persona> byEmailAndPassword = personaRepository.findByEmailAndPassword(requestLogin.getEmail(), requestLogin.getPassword());
        if (byEmailAndPassword.isPresent()){
            return true;
        }else{
            return false;
        }
    }
}
