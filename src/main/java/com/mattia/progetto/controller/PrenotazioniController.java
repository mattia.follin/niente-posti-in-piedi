package com.mattia.progetto.controller;

import com.mattia.progetto.model.Classe;
import com.mattia.progetto.model.Persona;
import com.mattia.progetto.repository.ClasseRepository;
import com.mattia.progetto.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrenotazioniController {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private ClasseRepository classeRepository;



    @PutMapping("/prenota/{id}/{idC}")
    ResponseEntity<?> updatePersona(@PathVariable Long id, @PathVariable Long idC){
        Persona personaToUpdate;
        try{
            personaToUpdate = personaRepository.findById(id).orElseThrow();
        }catch (Exception e){return new ResponseEntity<>("Utente non trovato", HttpStatus.NOT_FOUND);}

        Classe classeToUpdate;
        try{
            classeToUpdate = classeRepository.findById(idC).orElseThrow();
        }catch (Exception e){return new ResponseEntity<>("Classe non trovata", HttpStatus.NOT_FOUND);}

        personaToUpdate.setClasse(classeToUpdate);

        return new ResponseEntity<>(personaRepository.save(personaToUpdate),HttpStatus.OK);
    }

    @PutMapping("/prenota2/{id}/{idC}")
    ResponseEntity<?> removePersonaFromClass(@PathVariable Long id, @PathVariable Long idC){
        Persona personaToUpdate;
        try{
            personaToUpdate = personaRepository.findById(id).orElseThrow();
        }catch (Exception e){return new ResponseEntity<>("Utente non trovato", HttpStatus.NOT_FOUND);}

        Classe classeToUpdate;
        try{
            classeToUpdate = classeRepository.findById(idC).orElseThrow();

        }catch (Exception e){return new ResponseEntity<>("Classe non trovata", HttpStatus.NOT_FOUND);}
        personaToUpdate.setClasse(null);

        return new ResponseEntity<>(personaRepository.save(personaToUpdate),HttpStatus.OK);
    }
}
