package com.mattia.progetto.controller;

import com.mattia.progetto.model.Classe;
import com.mattia.progetto.model.Persona;
import com.mattia.progetto.model.RequestCreateClass;
import com.mattia.progetto.repository.ClasseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClasseController {

    private final ClasseRepository classeRepository;

    @Autowired
    ClasseController(ClasseRepository repository){
        classeRepository = repository;
    }

    @GetMapping("/classi")
    Iterable<Classe> getClassi() {
        return classeRepository.findAll();
    }

    @GetMapping("/classi/{idC}")
    Classe getClasse(@PathVariable Long idC) {
        return classeRepository.findById(idC).orElseThrow();
    }

    @GetMapping("/scelta/{id}")
    Classe getScelta(@PathVariable Long idC) {
        return classeRepository.findById(idC).orElseThrow();
    }

    @PostMapping("/classi")
    ResponseEntity<?> createClasse(@RequestBody RequestCreateClass requestCreateClass) {
        Persona persona = requestCreateClass.getPersona();
        Classe classe = requestCreateClass.getClasse();
        if (persona.getRuolo().equalsIgnoreCase("Admin"))
        return new ResponseEntity<>(classeRepository.save(classe), HttpStatus.OK);
        else{
            return new ResponseEntity<>("Non hai i permessi!!!", HttpStatus.UNAUTHORIZED);
        }
    }
}